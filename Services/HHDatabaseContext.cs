﻿using System;
using HHParser.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace HHParser.Services
{
    public class HhDatabaseContext : DbContext
    {
        public HhDatabaseContext(DbContextOptions<HhDatabaseContext> opts)
            : base(opts) { }

        public DbSet<Category> Categories { get; set; }
        public DbSet<Subcategory> Subcategories { get; set; }
        public DbSet<Vacancy> Vacancies { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            var uriConverter = new ValueConverter<Uri, string>(
                u => u.ToString(),
                u => new Uri(u));

            modelBuilder
                .Entity<Category>()
                .Property(c => c.Url)
                .HasConversion(uriConverter);

            modelBuilder
                .Entity<Subcategory>()
                .HasOne(s => s.Category)
                .WithMany(c => c.Subcategories)
                .OnDelete(DeleteBehavior.Cascade);
            modelBuilder
                .Entity<Subcategory>()
                .Property(s => s.Url)
                .HasConversion(uriConverter);

            modelBuilder
                .Entity<Vacancy>()
                .HasOne(v => v.Subcategory)
                .WithMany(s => s.Vacancies)
                .OnDelete(DeleteBehavior.Cascade);
            modelBuilder
                .Entity<Vacancy>()
                .Property(c => c.Url)
                .HasConversion(uriConverter);

        }
    }

}
