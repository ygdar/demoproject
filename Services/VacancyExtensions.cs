﻿using System.Text.RegularExpressions;

namespace HHParser.Services
{
    public static class VacancyExtensions
    {
        /// <summary>
        /// Анализирует строку и извлекает кортеж с начальным и конечным предложением по зарплате
        /// </summary>
        /// <param name="compensation"></param>
        /// <returns></returns>
        public static (int? init, int? final) ExtractCompensation(this string compensation)
        {
            var initPattern = new Regex(@"от (?<init>[\d\s]+)\D*");
            var finalPattern = new Regex(@"до (?<final>[\d\s]+)\D*");
            var initFinalPattern = new Regex(@"\D*(?<init>[\d\s]+)\D*(?<final>[\d\s]+)\D*");

            // В строке указано начальное предложение
            if (initPattern.IsMatch(compensation))
            {
                if (int.TryParse(GetCompensationString(initPattern, "init", compensation), out var initComp))
                    return (initComp, null);
            }

            // В строке указано конечное предложение
            if (finalPattern.IsMatch(compensation))
            {
                if (int.TryParse(GetCompensationString(finalPattern, "final", compensation), out var finalComp))
                    return (null, finalComp);
            }

            // В строке указано начальное и конечное предложение
            if (initFinalPattern.IsMatch(compensation))
            {
                int? initComp = null;
                int? finalComp = null;

                if (int.TryParse(GetCompensationString(initFinalPattern, "init", compensation), out var init))
                    initComp = init;

                if (int.TryParse(GetCompensationString(initFinalPattern, "final", compensation), out var final))
                    finalComp = final;

                return (initComp, finalComp);
            }

            return (null, null);
        }

        private static string GetCompensationString(Regex pattern, string group, string data)
        {
            var value = pattern.Match(data).Groups[group].Value;
            var spacePattern = new Regex(@"\s*");
            return spacePattern.Replace(value, string.Empty);
        }
    }
}
