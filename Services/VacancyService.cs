﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HHParser.Models;
using HtmlAgilityPack;
using Microsoft.EntityFrameworkCore;
using Z.EntityFramework.Plus;

namespace HHParser.Services
{
    public class VacancyService : IVacancyService
    {
        private readonly HhDatabaseContext _hhDatabaseContext;

        public VacancyService(HhDatabaseContext databaseContext)
        {
            _hhDatabaseContext = databaseContext;
        }

        /// <summary>
        /// Извлекает все категории, подкатегории и вакансии из базы
        /// (entity framework не поддерживает фильтрацию в конструкциях ThenInclude)
        /// </summary>
        /// <param name="vacancySearchString"></param>
        /// <returns></returns>
        public IList<Category> GetAllCategories(string vacancySearchString)
        {
            return _hhDatabaseContext.Categories
                .Select(category => new Category
                {
                    Id = category.Id,
                    Title = category.Title,
                    Url = category.Url,
                    Subcategories = category.Subcategories
                        .Select(subcategory => new Subcategory
                        {
                            Id = subcategory.Id,
                            Category = category,
                            Title = subcategory.Title,
                            Url = subcategory.Url,
                            Vacancies = subcategory.Vacancies
                                .Where(vacancy => string.IsNullOrEmpty(vacancySearchString) 
                                                  || vacancy.Title.Contains(vacancySearchString))
                                .OrderBy(vacancy => vacancy.Title)
                                .ToList()
                        })
                        .OrderBy(subcategory => subcategory.Title)
                        .ToList()
                })
                .OrderBy(category => category.Title)
                .ToList();
        }

        /// <summary>
        /// Удаляет категорию и все вложенные подкатегории по id
        /// </summary>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        public async Task DeleteCategory(int categoryId)
        {
            await _hhDatabaseContext
                .Categories
                .Where(category => category.Id == categoryId)
                .DeleteAsync();
        }

        /// <summary>
        /// Удаляет подкатегорию со всеми вакансиями по id
        /// </summary>
        /// <param name="subcategoryId"></param>
        /// <returns></returns>
        public async Task DeleteSubcategory(int subcategoryId)
        {
            await _hhDatabaseContext
                .Subcategories
                .Where(category => category.Id == subcategoryId)
                .DeleteAsync();
        }

        /// <summary>
        /// Перезагружает список категорий (без подкатегорий)
        /// </summary>
        /// <returns>Список категорий</returns>
        public async Task<IList<Category>> ReloadCategories()
        {
            var web = new HtmlWeb();
            var document = await web.LoadFromWebAsync(VacancySettings.CategoryCatalogAddress.ToString());

            var categoryNodes = document.DocumentNode?.SelectNodes(VacancySettings.CategoryDocumentSelector);

            if (categoryNodes == null)
                return new List<Category>();

            // Категории извлекаются из узлов и собираются в список моделей
            var categories = categoryNodes
                .Where(node => node != null)
                .Select(node => new Category
                {
                    Title = node.InnerText,
                    Url = new Uri(VacancySettings.DefaultUrlAddress, node.GetAttributeValue("href", ""))
                })
                .ToList();

            await _hhDatabaseContext.Categories.DeleteAsync();

            await _hhDatabaseContext.Categories.AddRangeAsync(categories);
            await _hhDatabaseContext.SaveChangesAsync();

            return categories;
        }

        /// <summary>
        /// Перезагружает список подкатегорий для заданной категории
        /// </summary>
        /// <param name="categoryId">Id категории</param>
        /// <returns>Список подкатегорий</returns>
        public async Task<IList<Subcategory>> ReloadSubcategories(int categoryId)
        {
            var category = await _hhDatabaseContext.Categories
                .Where(c => c.Id == categoryId)
                .Include(s => s.Subcategories)
                .FirstOrDefaultAsync();
            
            if (category == null)
                return new List<Subcategory>();

            var web = new HtmlWeb();
            var document = await web.LoadFromWebAsync(category.Url.ToString());

            var subcategoryNodes = document.DocumentNode?.SelectNodes(VacancySettings.SubcategoryDocumentSelector);

            if (subcategoryNodes == null)
                return new List<Subcategory>();

            var subcategories = subcategoryNodes
                .Where(node => node != null)
                .Select(node => new Subcategory
                {
                    Title = node.InnerText,
                    Url = new Uri(VacancySettings.DefaultUrlAddress, node.GetAttributeValue("href", ""))
                })
                .ToList();

            if (category.Subcategories != null)
                _hhDatabaseContext.Subcategories.RemoveRange(category.Subcategories);

            category.Subcategories = subcategories;
            await _hhDatabaseContext.SaveChangesAsync();

            return subcategories;
        }

        public async Task<IList<Vacancy>> ReloadVacancies(int subcategoryId)
        {
            var subcategory = await _hhDatabaseContext
                .Subcategories
                .Where(c => c.Id == subcategoryId)
                .Include(s => s.Vacancies)
                .FirstOrDefaultAsync();

            if (subcategory == null)
                return new List<Vacancy>();

            var web = new HtmlWeb();
            var document = await web.LoadFromWebAsync(subcategory.Url.ToString());

            var vacancies = ExtractVacanciesFromDocument(document) as List<Vacancy>;

            if (vacancies == null)
                return new List<Vacancy>();

            // Выбираем через пагинацию все страницы с вакансиями (за исключением первой)
            var pageLinksNodes = document.DocumentNode.SelectNodes(VacancySettings.SubcategoryPaginationLinks);

            // Если присутствует несколько страниц, извлекаем вакансии оттуда
            if (pageLinksNodes != null)
            {
                var paginationVacancies = pageLinksNodes
                    .Select(node => new Uri(VacancySettings.DefaultUrlAddress, node.GetAttributeValue("href", "")))
                    .Select(async link => await web.LoadFromWebAsync(link.ToString()))
                    .SelectMany(page => ExtractVacanciesFromDocument(page.Result));

                vacancies.AddRange(paginationVacancies);
            }

            // Средняя начальная цена
            var meanInitCompensation = (float) vacancies
                .Where(v => v.InitOffer.HasValue)
                .Select(v => v.InitOffer.Value)
                .DefaultIfEmpty(0)
                .Average();

            // Средняя конечная цена
            var meanFinalCompensation = (float) vacancies
                .Where(v => v.FinalOffer.HasValue)
                .Select(v => v.FinalOffer.Value)
                .DefaultIfEmpty(0)
                .Average();

            // Отбрасываются вакансии, чья цена отличается от средней в определённое число раз
            var filteredVacancies = vacancies
                .Where(v => !v.InitOffer.HasValue
                            || Math.Abs(meanInitCompensation - v.InitOffer.Value) / meanInitCompensation < VacancySettings.VacancyCompensationDeviation)
                .Where(v => !v.FinalOffer.HasValue
                            || Math.Abs(meanFinalCompensation - v.FinalOffer.Value) / meanFinalCompensation < VacancySettings.VacancyCompensationDeviation);

            if(subcategory.Vacancies != null)
                _hhDatabaseContext.Vacancies.RemoveRange(subcategory.Vacancies);
            subcategory.Vacancies = filteredVacancies.ToList();
            await _hhDatabaseContext.SaveChangesAsync();

            return vacancies;
        }

        /// <summary>
        /// Извлекает вакансии из документа
        /// </summary>
        /// <param name="document">HTML документ</param>
        /// <returns>Список вакансий</returns>
        protected IList<Vacancy> ExtractVacanciesFromDocument(HtmlDocument document)
        {
            var vacancyCards = document?.DocumentNode.SelectNodes(VacancySettings.VacancyCardSelector);
            if (vacancyCards == null)
                return new List<Vacancy>();

            return vacancyCards
                .Where(card => card != null)
                .Select(card =>
                {
                    // Данные по вакансии собираются в модель
                    var title = card.SelectSingleNode(VacancySettings.VacancyTitleSelector)?.InnerHtml ?? "";
                    var url = card.SelectSingleNode(VacancySettings.VacancyTitleSelector).GetAttributeValue("href", "");
                    var delta = card.SelectSingleNode(VacancySettings.VacancyDeltaSelector)?.InnerHtml ?? "";

                    var (initOffer, finalOffer) = delta.ExtractCompensation();

                    return new Vacancy
                    {
                        Title = title,
                        Url = new Uri(url),
                        InitOffer = initOffer,
                        FinalOffer = finalOffer
                    };
                })
                .ToList();
        }
        
    }
}
