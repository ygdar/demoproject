﻿using System.Collections.Generic;
using System.Threading.Tasks;
using HHParser.Models;

namespace HHParser.Services
{
    public interface IVacancyService
    {
        IList<Category> GetAllCategories(string vacancySearchString);

        Task DeleteCategory(int categoryId);

        Task DeleteSubcategory(int subcategoryId);

        Task<IList<Category>> ReloadCategories();

        Task<IList<Subcategory>> ReloadSubcategories(int categoryId);

        Task<IList<Vacancy>> ReloadVacancies(int subcategoryId);
    }
}
