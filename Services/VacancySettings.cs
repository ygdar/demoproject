﻿using System;

namespace HHParser.Services
{
    public static class VacancySettings
    {
        public static Uri DefaultUrlAddress => new Uri("https://yaroslavl.hh.ru");

        public static Uri CategoryCatalogAddress => new Uri(DefaultUrlAddress, "catalog");
        
        /// <summary>
        /// Шаблон для извлечения категорий
        /// </summary>
        public static string CategoryDocumentSelector => "//a[@class='catalog__item-link']";

        /// <summary>
        /// Шаблон для извлечения подкатегорий
        /// </summary>
        public static string SubcategoryDocumentSelector =>
            "//div[@class='bloko-toggle__expandable-reverse']//a[@class='catalog__item-link']";

        /// <summary>
        /// Шаблон для извлечения ссылок из пагинации
        /// </summary>
        public static string SubcategoryPaginationLinks => "//a[@data-qa='pager-page']";

        /// <summary>
        /// Шаблон для извлечения карточки вакансии
        /// </summary>
        public static string VacancyCardSelector => "//div[@data-qa='vacancy-serp__vacancy']";

        /// <summary>
        /// Шаблон для извлечения заголовка вакансии
        /// </summary>
        public static string VacancyTitleSelector => ".//a[@data-qa='vacancy-serp__vacancy-title']";

        /// <summary>
        /// Шаблон для извлечения зарплатной вилки
        /// </summary>
        public static string VacancyDeltaSelector => ".//div[@data-qa='vacancy-serp__vacancy-compensation']";

        /// <summary>
        /// Максимально допустимое отклонение предложения по вакансии от среднего
        /// </summary>
        public static float VacancyCompensationDeviation => 0.6f;
    }
}
