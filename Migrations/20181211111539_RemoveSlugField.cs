﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace HHParser.Migrations
{
    public partial class RemoveSlugField : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Slug",
                table: "Subcategories");

            migrationBuilder.DropColumn(
                name: "Slug",
                table: "Categories");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Slug",
                table: "Subcategories",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Slug",
                table: "Categories",
                nullable: true);
        }
    }
}
