﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using HHParser.Services;

namespace HHParser.Controllers
{
    public class HomeController : Controller
    {
        private readonly IVacancyService _vacancyService;

        public HomeController(IVacancyService vacancyService)
        {
            _vacancyService = vacancyService;
        }

        public IActionResult Home(string searchVacancyName)
        {
            var categories = _vacancyService.GetAllCategories(searchVacancyName);
            return View(categories);
        }

        public async Task<IActionResult> ReloadCategories()
        {
            await _vacancyService.ReloadCategories();

            return RedirectToAction(nameof(Home));
        }

        public async Task<IActionResult> ReloadSubcategories(int? categoryId)
        {
            if (categoryId.HasValue)
                await _vacancyService.ReloadSubcategories(categoryId.Value);

            return RedirectToAction(nameof(Home));
        }

        public async Task<IActionResult> ReloadVacancies(int? subcategoryId)
        {
            if (subcategoryId.HasValue)
                await _vacancyService.ReloadVacancies(subcategoryId.Value);

            return RedirectToAction(nameof(Home));
        }

        public async Task<IActionResult> DeleteCategory(int? categoryId)
        {
            if (categoryId.HasValue)
                await _vacancyService.DeleteCategory(categoryId.Value);

            return RedirectToAction(nameof(Home));
        }

        public async Task<IActionResult> DeleteSubcategory(int? subcategoryId)
        {
            if (subcategoryId.HasValue)
                await _vacancyService.DeleteSubcategory(subcategoryId.Value);

            return RedirectToAction(nameof(Home));
        }
    }
}
